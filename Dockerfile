FROM centos:7

MAINTAINER Qiang Kou <qiang.kou@h2o.ai>

RUN yum update -y && \
yum install -y centos-release-scl-rh && \
yum install -y devtoolset-7-gcc-c++ && \
yum install -y wget && \
yum install -y make && \
yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm && \
yum install -y gtest gtest-devel && \
yum install -y lcov && \
yum install -y git && \
yum install -y valgrind && \
yum install -y libicu-devel && \
yum install -y openssl-devel && \
yum install -y zlib-devel minizip-devel && \
yum -y install https://centos7.iuscommunity.org/ius-release.rpm && \
yum -y install python36u-devel python36u-pip && \
yum -y install cmake && \
yum clean all

# set environment variables.
ENV HOME=/root \
    PATH=/opt/rh/devtoolset-7/root/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# python
RUN \
    pip3.6 install --upgrade pip && pip3.6 install setuptools numpy pandas scikit-learn virtualenv

# install protobuf
RUN \
    wget https://github.com/protocolbuffers/protobuf/releases/download/v3.6.1/protobuf-all-3.6.1.tar.gz && \
    tar zxvf protobuf-all-3.6.1.tar.gz && \
    cd protobuf-3.6.1 && \
    CXXFLAGS=-fPIC ./configure && make install && ldconfig /usr/local/lib && \
    cd python && python3.6 setup.py build && python3.6 setup.py install && \
    cd ../.. && rm -rf protobuf-3.6.1 protobuf-all-3.6.1.tar.gz

# build xgboost
RUN \
    git clone https://github.com/h2oai/xgboost.git && \
    cd xgboost && \
    git checkout 0b6a96db94c723132e058c331979bf69404b864d && \
    git submodule update --init && \
    make && cd python-package/ && \
    python3.6 setup.py build && python3.6 setup.py install && \
    cd ../../ && rm -rf xgboost

# build lightgbm
RUN \
    git clone https://github.com/h2oai/LightGBM.git && \
    cd LightGBM && \
    git checkout a769f725a7dabf2a305d7278d8c67183b6842552 && \
    git submodule update --init && \
    mkdir build && cd build && \
    cmake .. && make && \
    cd ../python-package && \
    python3.6 setup.py install && \
    cd ../../ && rm -rf LightGBM

# Define working directory.
WORKDIR /root

# Define default command.
CMD ["bash"]
